import {Component, OnInit} from '@angular/core';
import {FormsService} from "../forms-service.service";
import {FormControl} from "@angular/forms";
import {MatTableDataSource} from "@angular/material/table";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  initiator = new FormControl('');
  displayedColumns: string[] = ['initiator', 'csvFileName', 'pdfFileName', 'status' ,'actions'];
  displayedColumnsUnprocessed: string[] = ['name'];
  processedForms = [];
  unprocessedForms: any;

  constructor(private formsService: FormsService) {
  }

  ngOnInit(): void {
    this.initialLoad();
  }

  initialLoad(): void {
    this.formsService.getForms().subscribe(response => {
      this.processedForms = response.processedForms;
      this.unprocessedForms = new MatTableDataSource<any>(response.unprocessedForms);
      console.log(response);
    });
  }

  triggerProcessing(): void {
    this.formsService.triggerProcessing(this.initiator.value).subscribe(response => {
      this.initialLoad();
    })
  }

  downloadPDF(element: any): void {
    // @ts-ignore
     this.formsService.downloadPDF(element.pdfFileName);
  }



}
