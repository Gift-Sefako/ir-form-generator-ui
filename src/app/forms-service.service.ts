import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class FormsService {

  private baseUrl = 'http://localhost:8080/forms/';

  constructor(private http: HttpClient) { }

  getForms(): Observable<any> {
    return this.http.get(`${this.baseUrl}`+'processed-forms');
  }

  triggerProcessing(initiator: string): Observable<any> {
    let httpParams = new HttpParams().append("initiator", initiator);
    return this.http.post(`${this.baseUrl}`+'trigger-processing', httpParams);
  }

  downloadPDF(pdfFilename : string) {
    let encodedURL= this.baseUrl +'download-file/?filename=' + encodeURIComponent(pdfFilename);
    // @ts-ignore
    window.open(encodedURL, '_blank').focus();
  }
}
